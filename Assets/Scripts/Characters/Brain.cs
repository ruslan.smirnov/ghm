﻿using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

namespace GHM.Game.Characters
{
    public sealed class Brain : MonoBehaviour, IPointerDownHandler
    {
        private const string ANIMATOR_SPEED_VALUE = "Speed";
        private const string ANIMATOR_HAPPY_TRIGGER = "Happy";

        [SerializeField]
        private BrainFSM _fsm = null;

        [SerializeField]
        private ServerQueue _serverQueue = null;

        [SerializeField]
        private BrainHome _home = null;

        [SerializeField]
        private float _patience = default;

        [SerializeField]
        private float _speed = default;

        [SerializeField]
        private SpriteMask _orderTimer = null;

        [SerializeField]
        private NavMeshAgent _agent = null;

        [SerializeField]
        private Animator _animator = null;

        [SerializeField]
        private SpriteRenderer _spriteRenderer = null;

        public BrainFSM FSM { get { return _fsm; } }
        public ServerQueue ServerQueue { get { return _serverQueue; } }

        public Vector3 HomePosition { get; private set; }
        public float Patience { get { return _patience; } }

        public Color Color { get { return _spriteRenderer.color; } }

        public Action WalkCompleted;

        private bool _isWalk = false;

        public void SetTimerActive(bool value)
        {
            _orderTimer.gameObject.SetActive(value);
        }

        public void SetTimerAlphaCutoff(float value)
        {
            _orderTimer.alphaCutoff = value;
        }

        public void ToDirectHome()
        {
            FSM.Touch();
        }

        public void RunWalk(Vector3 position)
        {
            _agent.destination = position;
            _agent.updateRotation = false;
            _agent.speed = _speed;

            _isWalk = true;
        }

        public void StopWalk()
        {
            _agent.speed = 0;
            _animator.SetFloat(ANIMATOR_SPEED_VALUE, 0);
            _isWalk = false;
        }

        public void ShowEmote()
        {
            _animator.SetTrigger(ANIMATOR_HAPPY_TRIGGER);
        }

        private void Awake()
        {
            FSM.Setup(this);

            HomePosition = _home.transform.position;
        }

        private void Start()
        {
            FSM.ChangeState(BrainFSM.States.Init);

            SetTimerActive(false);
        }

        private void Update()
        {
            if (!_isWalk)
            {
                return;
            }

            if (_agent.pathPending)
            {
                return;
            }

            if (_agent.remainingDistance > _agent.stoppingDistance)
            {
                _animator.SetFloat(ANIMATOR_SPEED_VALUE, _agent.speed);
            }
            else
            {
                StopWalk();
                WalkCompleted?.Invoke();
            }
        }

        public void OnPointerDown(PointerEventData pointerEventData)
        {
            FSM.Touch();
        }
    }
}