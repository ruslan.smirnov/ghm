﻿using UnityEngine;

namespace GHM.Game.Characters
{
    public sealed class BrainFSM : MonoBehaviour
    {
        private BrainState _currentState;
        private States _currentStateType;

        private Brain _character = null;

        public States CurrentState 
        { 
            get { return _currentStateType; }
        }

        public void Setup(Brain character)
        {
            _character = character;
            _currentStateType = States.Idle;
        }

        public void ChangeState(States state)
        {
            switch (state)
            {
                case States.Init:
                    _currentState = new Init(_character);
                    _currentStateType = States.Init;
                    break;
                case States.Idle:
                    _currentState = new Idle(_character);
                    _currentStateType = States.Idle;
                    break;
                case States.WalkToServer:
                    _currentState = new WalkToServer(_character);
                    _currentStateType = States.WalkToServer;
                    break;
                case States.InQueue:
                    _currentState = new InQueue(_character);
                    _currentStateType = States.InQueue;
                    break;                    
                case States.WaitingServer:
                    _currentState = new WaitingServer(_character);
                    _currentStateType = States.WaitingServer;
                    break;
                case States.PlayEmote:
                    _currentState = new PlayEmote(_character);
                    _currentStateType = States.PlayEmote;
                    break;                    
                case States.WalkToHome:
                    _currentState = new WalkToHome(_character);
                    _currentStateType = States.WalkToHome;
                    break;
                default:
                    break;
            }
        }

        public void Update()
        {
            _currentState.Update();
        }

        public void Touch()
        {
            _currentState.OnTouch();
        }

        public enum States
        {
            Init,
            Idle,
            WalkToServer,
            InQueue,
            WaitingServer,
            PlayEmote,
            WalkToHome,
        }
    }
}