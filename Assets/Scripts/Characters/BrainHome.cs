﻿using UnityEngine;
using UnityEngine.EventSystems;
using GHM.Game.Characters;

namespace GHM.Game
{
    public sealed class BrainHome : MonoBehaviour, IPointerDownHandler
    {
        private const float COLOR_MODIFIER = 0.66f;

        [SerializeField]
        private SpriteRenderer _spriteRenderer = null;

        [SerializeField]
        private Brain _brain = null;

        public void Start()
        {
            _spriteRenderer.color = _brain.Color * COLOR_MODIFIER;
        }

        public void OnPointerDown(PointerEventData pointerEventData)
        {
            _brain.ToDirectHome();
        }
    }
}