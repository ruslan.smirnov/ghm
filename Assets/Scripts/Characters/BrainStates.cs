﻿using UnityEngine;
using GHM.Core;

namespace GHM.Game.Characters
{
    public abstract class BrainState
    {
        public BrainState(Brain character)
        {
            Character = character;
        }

        public Brain Character { get; }

        public virtual void Update()
        {

        }

        public virtual void OnTouch()
        {

        }
    }

    public sealed class Init : BrainState
    {
        public Init(Brain character) : base(character)
        {
        }

        public override void Update()
        {
            Character.FSM.ChangeState(BrainFSM.States.Idle);
        }
    }

    public sealed class Idle : BrainState
    {
        public Idle(Brain character) : base(character)
        {
        }

        public override void OnTouch()
        {
            Character.FSM.ChangeState(BrainFSM.States.WalkToServer);
        }
    }

    public sealed class WalkToServer : BrainState
    {
        public WalkToServer(Brain character) : base(character)
        {
            character.WalkCompleted += OnWalkCompleted;
            character.RunWalk(character.ServerQueue.WaitingPositionSlot);
        }

        public override void OnTouch()
        {
            Character.StopWalk();
            Character.WalkCompleted -= OnWalkCompleted;
            Character.FSM.ChangeState(BrainFSM.States.WalkToHome);
        }

        private void OnWalkCompleted()
        {
            Character.WalkCompleted -= OnWalkCompleted;
            Character.FSM.ChangeState(BrainFSM.States.InQueue);
        }
    }

    public sealed class InQueue : BrainState
    {
        private bool _isSendedMsg = false;

        public InQueue(Brain character) : base(character)
        {
        }

        public override void Update()
        {
            if (!_isSendedMsg)
            {
                _isSendedMsg = true;
                MessageSystem.Instance.Notify(new CharacterArrivedToServerMsg(Character));
            }
        }

        public override void OnTouch()
        {
            MessageSystem.Instance.Notify(new CharacterLeaveQueueMsg(Character));
            Character.FSM.ChangeState(BrainFSM.States.WalkToHome);
        }
    }    

    public sealed class WaitingServer : BrainState
    {
        private float _waitingTimer = 0;

        public WaitingServer(Brain character) : base(character)
        {
            character.SetTimerActive(true);
            _waitingTimer = character.Patience;
        }

        public override void Update()
        {
            if (_waitingTimer > 0)
            {
                _waitingTimer -= Time.deltaTime;

                Character.SetTimerAlphaCutoff(_waitingTimer / Character.Patience);
            }
            else
            {
                Character.SetTimerActive(false);

                MessageSystem.Instance.Notify(new CharacterTimerOverMsg(Character));

                Character.FSM.ChangeState(BrainFSM.States.WalkToHome);
            }
        }

        public override void OnTouch()
        {
            Character.SetTimerActive(false);
            Character.FSM.ChangeState(BrainFSM.States.WalkToHome);

            MessageSystem.Instance.Notify(new CharacterLeaveQueueMsg(Character));
        }
    }

    public sealed class PlayEmote : BrainState
    {
        public PlayEmote(Brain character) : base(character)
        {
            character.ShowEmote();
            character.SetTimerActive(false);
        }

        public override void OnTouch()
        {
            MessageSystem.Instance.Notify(new CharacterHappyLeaveQueueMsg(Character));
            Character.FSM.ChangeState(BrainFSM.States.WalkToHome);
        }
    }    

    public sealed class WalkToHome : BrainState
    {
        public WalkToHome(Brain character) : base(character)
        {
            character.WalkCompleted += OnWalkCompleted;
            character.RunWalk(character.HomePosition);
        }

        public override void OnTouch()
        {
            Character.WalkCompleted -= OnWalkCompleted;
            Character.StopWalk();            
            Character.FSM.ChangeState(BrainFSM.States.WalkToServer);
        }

        private void OnWalkCompleted()
        {
            Character.WalkCompleted -= OnWalkCompleted;
            Character.FSM.ChangeState(BrainFSM.States.Idle);
        }
    }
}
