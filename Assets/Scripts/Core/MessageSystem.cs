﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace GHM.Core
{
    public interface IGameMessage
    {
    }

    public sealed class MessageSystem
    {
        private const string DEBUG_MSG_MESSAGE_TYPE_HASNT_FOUND = "MessageSystem: can't unsubscribe because a IGameMessage has't found {0}";
        private const string DEBUG_MSG_ALREADY_ADDED_TO_SUBSCRIBE = "MessageSystem: can't subscribe because a subscriber with same signanture already added to subscribe {0} {1} {2}";
        private const string DEBUG_MSG_ALREADY_ADDED_TO_REMOVABLE = "MessageSystem: can't unsubscribe because a subscriber with same signanture already added to remove {0} {1} {2}";
        private const string DEBUG_MSG_NULLABE_SUBSCRIBER = "MessageSystem: Has been find nullable subscriber {0} {1}";

        public static MessageSystem Instance = new MessageSystem();

        private readonly Dictionary<Type, List<Subscriber>> _subscribers =
            new Dictionary<Type, List<Subscriber>>();

        private readonly Dictionary<Type, List<Subscriber>> _removableSubscribes =
            new Dictionary<Type, List<Subscriber>>();

        private bool _canClearUp = false;

        private MessageSystem()
        {
        }

        public void Notify<T>(T gameMessage) where T : IGameMessage
        {
            if (_canClearUp)
            {
                RemoveSubscribers();
                _canClearUp = false;
            }

            var messageType = typeof(T);
            if (!_subscribers.ContainsKey(messageType))
            {
                return;
            }

            var subscribers = _subscribers[messageType];

            foreach (var item in subscribers)
            {
                // add to remove if object destroyed
                if (item.Owner.Equals(null))
                {
                    Debug.Log(string.Format(DEBUG_MSG_NULLABE_SUBSCRIBER, 
                        messageType.Name, item.Owner.GetType().Name));

                    _removableSubscribes[messageType].Add(item);
                    _canClearUp = true;
                }
                else
                {
                    var callback = item.Handle as Action<T>;
                    callback.Invoke(gameMessage);
                }
            }
        }

        public void Subscribe<T>(Action<T> callback) where T : IGameMessage
        {
            if (_canClearUp)
            {
                RemoveSubscribers();
                _canClearUp = false;
            }

            var messageType = typeof(T);

			if (!_subscribers.ContainsKey(messageType))
            {
                _subscribers.Add(messageType, new List<Subscriber>());
                _removableSubscribes.Add(messageType, new List<Subscriber>());
            }

            var subscriber = FindSubscriber(messageType, callback.Target);

            if (subscriber != null && subscriber.Handle.Method == callback.Method)
            {
                Debug.Log(string.Format(DEBUG_MSG_ALREADY_ADDED_TO_SUBSCRIBE,
                    callback.Target.ToString(), callback.Target.GetType().Name, callback.Method.Name));
            }
            else
            {
                _subscribers[messageType].Add(new Subscriber(callback.Target, callback));
            }
        }

        public void Unsubscribe<T>(Action<T> callback) where T : IGameMessage
        {
            var messageType = typeof(T);
            if (!_subscribers.ContainsKey(messageType))
            {
                return;
            }

            var subscriber = FindSubscriber(messageType, callback.Target);

            if (subscriber != null && subscriber.Handle.Method == callback.Method)
            {
                if (!_removableSubscribes[messageType].Contains(subscriber))
                {
                    _removableSubscribes[messageType].Add(subscriber);
                    _canClearUp = true;
                }
                else
                {
                    Debug.Log(string.Format(DEBUG_MSG_ALREADY_ADDED_TO_REMOVABLE,
                        callback.Target.ToString(), callback.Target.GetType().Name, callback.Method.Name));
                }
            }
        }

        private Subscriber FindSubscriber(Type messageType, object subscriber)
        {
            return _subscribers[messageType].Find(x => x.Owner == subscriber);
        }

        private void RemoveSubscribers()
        {
            foreach (var item in _removableSubscribes)
            {
                var messageType = item.Key;
                var collection = item.Value;

                if (item.Value.Count > 0)
                {
                    foreach (Subscriber removableSubscriber in collection)
                    {
                        _subscribers[messageType].Remove(removableSubscriber);
                    }

                    _removableSubscribes[messageType].Clear();
                }
            }
        }

        private sealed class Subscriber
		{
            public readonly object Owner;
            public readonly Delegate Handle;

            public Subscriber(object subscriber, Delegate action)
            {
                Owner = subscriber;
                Handle = action;
            }
        }
	}
}

