﻿using UnityEngine;
using GHM.Core;
using GHM.Game.Characters;

namespace GHM.Game
{
    public class EmoteState : StateMachineBehaviour
    {
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            var character = animator.GetComponent<Brain>();

            MessageSystem.Instance.Notify(new CharacterHappyLeaveQueueMsg(character));
            character.FSM.ChangeState(BrainFSM.States.WalkToHome);
        }
    }
}

