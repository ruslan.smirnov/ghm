﻿using GHM.Core;
using GHM.Game.Characters;

namespace GHM.Game
{
    public sealed class CharacterArrivedToServerMsg : IGameMessage
    {
        public Brain Character { get; private set; }

        public CharacterArrivedToServerMsg(Brain character)
        {
            Character = character;
        }
    } 

    public sealed class CharacterTimerOverMsg : IGameMessage
    {
        public Brain Character { get; private set; }

        public CharacterTimerOverMsg(Brain character)
        {
            Character = character;
        }
    }

    public sealed class CharacterLeaveQueueMsg : IGameMessage
    {
        public Brain Character { get; private set; }

        public CharacterLeaveQueueMsg(Brain character)
        {
            Character = character;
        }
    }

    public sealed class CharacterHappyLeaveQueueMsg : IGameMessage
    {
        public Brain Character { get; private set; }

        public CharacterHappyLeaveQueueMsg(Brain character)
        {
            Character = character;
        }
    }
}