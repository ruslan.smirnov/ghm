﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GHM.Core;
using GHM.Game.Characters;

namespace GHM.Game
{
    public sealed class ServerQueue : MonoBehaviour, IPointerDownHandler
    {
        private const float WAITING_POSITION_STEP = 0.5f;

        private readonly List<Brain> _characters = new List<Brain>();

        [SerializeField]
        private GameObject _point = null;

        private bool _isClickBlocked = false;
        
        public Vector3 WaitingPosition { get; private set; }     

        public Vector3 WaitingPositionSlot
        {
            get 
            { 
                return WaitingPosition
                + new Vector3(_characters.Count * WAITING_POSITION_STEP, 0, 0); 
            }
        }

        private void OnCharacterArrived(CharacterArrivedToServerMsg msg)
        {
            var character = msg.Character;

            if (_characters.Contains(character))
            {
                return;
            }

            if (_characters.Count == 0)
            {
                character.FSM.ChangeState(BrainFSM.States.WaitingServer);
            }

            _characters.Add(character);

            UpdateQueueView();
        }

        private void OnCharacterTimerOver(CharacterTimerOverMsg msg)
        {
            _characters.RemoveAt(0);

            if (_characters.Count == 0)
            {
                return;
            }

            UpdateQueueView();

            _characters[0].FSM.ChangeState(BrainFSM.States.WaitingServer);
        }

        private void OnCharacterLeaveQueue(CharacterLeaveQueueMsg msg)
        {
            int index = _characters.IndexOf(msg.Character);

            if (index == 0)
            {
                _characters.RemoveAt(0);

                if (_characters.Count > 0)
                {
                    _characters[0].FSM.ChangeState(BrainFSM.States.WaitingServer);
                }
            }
            else
            {
                _characters.RemoveAt(index);
            }

            UpdateQueueView();
        }

        private void CharacterHappyLeaveQueue(CharacterHappyLeaveQueueMsg msg)
        {
            _isClickBlocked = false;

            int index = _characters.IndexOf(msg.Character);

            if (index < 0)
            {
                return;
            }

            if (index == 0)
            {
                _characters.RemoveAt(0);

                if (_characters.Count > 0)
                {
                    _characters[0].FSM.ChangeState(BrainFSM.States.WaitingServer);
                }
            }
            else
            {
                _characters.RemoveAt(index);
            }

            UpdateQueueView();
        }

        private void UpdateQueueView()
        {
            for(int index = 0; index < _characters.Count; index++)
            {
                var walkPosition = WaitingPosition + new Vector3(index * 0.5f, 0, 0);
                _characters[index].RunWalk(walkPosition);
            }
        }

        private void Awake()
        {
            MessageSystem.Instance.Subscribe<CharacterArrivedToServerMsg>(OnCharacterArrived);
            MessageSystem.Instance.Subscribe<CharacterTimerOverMsg>(OnCharacterTimerOver);
            MessageSystem.Instance.Subscribe<CharacterLeaveQueueMsg>(OnCharacterLeaveQueue);
            MessageSystem.Instance.Subscribe<CharacterHappyLeaveQueueMsg>(CharacterHappyLeaveQueue);

            WaitingPosition = _point.transform.position;
        }

        private void Update()
        {

        }

        public void OnPointerDown(PointerEventData pointerEventData)
        {
            if (!_isClickBlocked && _characters.Count > 0)
            {
                _characters[0].FSM.ChangeState(BrainFSM.States.PlayEmote);
                _isClickBlocked = true;
            }
        }
    }
}